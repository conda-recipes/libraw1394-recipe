# libraw1394 conda recipe

Home: "https://ieee1394.wiki.kernel.org/"

Package license: LGPL

Recipe license: BSD 3-Clause

Summary: Linux IEEE 1394 (FireWire) kernel drivers
